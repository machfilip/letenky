# -*- coding: utf-8 -*-
import urllib
from urllib.parse import urlparse
import urllib.request
import re
from bs4 import BeautifulSoup
import mysql.connector
import sys
import os
from datetime import datetime
import syslog
import subprocess
import yaml
from inspect import getsourcefile
from urllib.request import Request, urlopen

syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.safe_load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['letenky']['username']
secret_password = conf['aws']['letenky']['password']
secret_database = conf['aws']['letenky']['database']
secret_host = conf['aws']['letenky']['host']
secret_port = conf['aws']['letenky']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()

url = 'https://obletsvet.cz'
category = 'Obletsvet.cz'

req = Request(url, headers={'User-Agent':'Mozilla/5.0'})
html = urlopen(req).read()

soup = BeautifulSoup(html, "html.parser")

links = soup.find_all('div', {"class":"ob-card s-2 sh-2"} )

for item in links:
	title = item.div.a['title']
	url_link = url+item.div.a['href']	
	price_html = item.find_all('div',{"class":"ob-tag rounded-pill d-inline-flex align-items-center flex-nowrap py-1 px-4 mb-2 s-1"})


	p = list()
	for price_i in price_html:
		print(price_i.string)
		p.append(str(price_i.string) )

	price = ''.join([str(elem) for elem in p])
	price = price.replace(" ", "")
	price = re.search(r'[0-9]+', price).group(0)


	print(price)	
	print(title)
	print(url_link)

	req = Request(url_link, headers={'User-Agent':'Mozilla/5.0'})
	html = urlopen(req).read()
	soup1 = BeautifulSoup(html, "html.parser")
	descr = soup1.find('div',{"class":"col-12 col-md-7 col-lg-8"}).p
	descr = str(descr)
	
	query = ("SELECT * FROM "+secret_database+".Record WHERE LINK = %s")
	cursor.execute(query, (url_link,) )
	rows = cursor.fetchall()
	success = 0
	if cursor.rowcount < 1:
		query = "INSERT INTO "+secret_database+".Record (TITLE, LINK, PRICE, DESCRIPTION, CATEGORY) VALUES (%s, %s, %s, %s, %s)"
		cursor.execute(query, (title, url_link, price, descr, category) )
		cnx.commit()

cursor.close()
cnx.close()

sys.exit()
