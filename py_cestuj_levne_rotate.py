# -*- coding: utf-8 -*-
import urllib
from urllib.parse import urlparse
import urllib.request
import re
from bs4 import BeautifulSoup
import mysql.connector
import sys
import os
from datetime import datetime
import syslog
import subprocess
import yaml
from inspect import getsourcefile


syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['letenky']['username']
secret_password = conf['aws']['letenky']['password']
secret_database = conf['aws']['letenky']['database']
secret_host = conf['aws']['letenky']['host']
secret_port = conf['aws']['letenky']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()

for x in range(40, 310):

	url = 'https://www.cestujlevne.com/akcni-letenky/strana-'+str(x)
	print(url)

	category = 'Cestujlevne.com'

	with urllib.request.urlopen(url) as response:
	   html = response.read()

	soup = BeautifulSoup(html, "html.parser")

	links = soup.find_all('div', {"class":"md__cnt"} )

	for item in links:

		try:
			cena = item.a.strong.get_text()
			cena = cena.replace(" ", "")
			cena = cena.replace(" ", "")
			cena = cena.replace("Kč","")
		except:
			cena = -1
	
		try:
			title = item.a.get_text().strip()
			title = title.replace(" ","")
			title = title.replace("Kč","")
			title = title.strip()
			title = re.sub(r"[0-9]+\s+", "", title)
		except:
			title = ''

		try:
			link = item.a.get('href').strip()
		except:
			link = ''

		if len(link)>1:
			with urllib.request.urlopen(link) as response:
				html = response.read()

			soup1 = BeautifulSoup(html, "html.parser")
			descr = soup1.find('div', {'class':'cl__main cl__main--campaign'} )
			par = descr.find_all('p', recursive=False)
			descr = "";
			for p in par:
				descr = descr+p.get_text()+"\n"

			time = soup1.find("div", {"class":"hdr__nf"} )
			time = time.contents[1].get_text().strip()
			time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
			s_time = datetime.strftime(time, "%Y-%m-%d")
		else:
			descr = ''
			time = '1700-01-01'

		query = ("SELECT * FROM "+secret_database+".Record WHERE LINK = %s")
		cursor.execute(query, (link,) )
		rows = cursor.fetchall()
		success = 0

		if cursor.rowcount < 1:
			query = "INSERT INTO "+secret_database+".Record (TITLE, LINK, PRICE, DESCRIPTION, CATEGORY, PUBLISHED_DATE) VALUES (%s, %s, %s, %s, %s, %s)"
			cursor.execute(query, (title, link, cena, descr, category, s_time) )
			print(cursor._executed)
			cnx.commit()

cursor.close()
cnx.close()

sys.exit()
