from functions import sendMail
import mysql.connector
import sys
import yaml
from inspect import getsourcefile
import os.path
import sys

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]


conf = yaml.safe_load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['letenky']['username']
secret_password = conf['aws']['letenky']['password']
secret_database = conf['aws']['letenky']['database']
secret_host = conf['aws']['letenky']['host']
secret_port = conf['aws']['letenky']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)

cursor = cnx.cursor()
query = ("SELECT * FROM "+secret_database+".V_Record ORDER BY Category, Title;")
cursor.execute(query)

rows = cursor.fetchall()
cursor.close()
cnx.close()

if len(rows) == 0:
	sys.exit()


message = "<html><body>"
for row in rows:
	content = row[3]
	content = content.replace(r'\"', r'"')
	link = "<p><a href='"+str(row[1])+"'>"+row[0]+"</a><br><br>"+content+"</p>"
	message = message + link
	message = message+"</body></html>"

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()
  
i = sendMail(message, row[0]+" od "+str(row[2]) )
if i == 1:
	query = ("UPDATE "+secret_database+".Record SET Sent_Flag = 'Y' WHERE Sent_Flag = 'N';")
	cursor.execute(query)
	cnx.commit()

cursor.close()
cnx.close()
