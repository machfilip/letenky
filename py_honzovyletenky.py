# -*- coding: utf-8 -*-
import urllib
from urllib.parse import urlparse
import urllib.request
import re
from bs4 import BeautifulSoup
import mysql.connector
import sys
import os
from datetime import datetime
from time import gmtime, strftime
import syslog
import subprocess
import yaml
from inspect import getsourcefile


syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.safe_load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['letenky']['username']
secret_password = conf['aws']['letenky']['password']
secret_database = conf['aws']['letenky']['database']
secret_host = conf['aws']['letenky']['host']
secret_port = conf['aws']['letenky']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()

url = 'https://www.honzovyletenky.cz/'

category = 'Honzovyletenky.cz'

with urllib.request.urlopen(url) as response:
   html = response.read()

soup = BeautifulSoup(html, "html.parser")

links = soup.find_all("div", {"class":"article-box"} )

for item in links:
	try:
		link = item.a.get('href')
		title = item.a.get('title')
		cena = item.find("span", {"class":"article-box__price"})
		cena = cena.string
		cena = cena.replace(" ", "")
		cena = re.search("[0-9]+", cena)
		cena = cena.group(0)
	
		print(link)
		print(title)
		print(cena)
	
		with urllib.request.urlopen(link) as response:
			html = response.read()
	
		soup1 = BeautifulSoup(html, "html.parser")
	
		try:
			descr = soup1.find('div', {'class':'perex'} )
			descr = str(descr)
			descr = "<html><body>"+str(descr)+"</body></html>"
		except:
			descr = ""
	
		s_time = strftime("%Y-%m-%d", gmtime())
	
		descr = descr.replace('"', r'\"')
		title = title.replace('"', r'\"')
	
		print(descr)

		query = ("SELECT * FROM "+secret_database+".Record WHERE LINK = %s")
		cursor.execute(query, (link,) )
		rows = cursor.fetchall()
		success = 0

		if cursor.rowcount < 1:
			query = "INSERT INTO "+secret_database+".Record (TITLE, LINK, PRICE, DESCRIPTION, CATEGORY, PUBLISHED_DATE) VALUES (%s, %s, %s, %s, %s, %s)"		
			cursor.execute(query, (title, link, cena, descr, category, s_time) )
			cnx.commit()
	except:
		print("Wrong item")

cursor.close()
cnx.close()

sys.exit()
